import * as fs from "fs";
import * as path from "path";
import * as child_process from "child_process";
import * as puppeteer from "puppeteer";
import * as marked from "marked";

const DEBUG = true;
const spawn = child_process.spawn;

type NpmViewResult = {
  name: string;
  description: string;
  homepage: string;
  version: string;
  engines?: {
    node?: string;
  };
};

type PkgInfo = {
  name: string;
  usingVersion: string;
  latestVersion: string;
  packageJsonValue: string;
  description: string;
  weeklyDownloads: string;
  latestRequiredNodeVersion: string;
  homepage: string;
};

type Result = {
  dependencies: PkgInfo[];
  devDependencies: PkgInfo[];
};

function consoleLog(log: any) {
  if (DEBUG) {
    console.log(log);
  }
}

function puppeteerClient() {
  let browser: puppeteer.Browser = null;
  let page: puppeteer.Page = null;

  return {
    getWeeklyDownloads: async (
      url: string,
      selector: string
    ): Promise<string> => {
      consoleLog(`open ${url}`);

      if (!browser) {
        browser = await puppeteer.launch({
          timeout: 100000
        });
        page = await browser.newPage();
      }
      await page.goto(url);

      const elem = await page.$(selector);

      try {
        
        const jsHandle = await elem.getProperty("textContent");
        const text = await jsHandle.jsonValue<string>();

        consoleLog(`${url} -> ${text}`);

        return text;
      } catch (err) {
        consoleLog(err);
        consoleLog(elem);
        consoleLog(`failed get weeklyDownloads from [ ${url} ]`);
        return "";
      }
    },
    close: async () => {
      if (browser) {
        await browser.close();
      }
    },
  };
}

async function execNpm<T>(args: string[], cwd: string): Promise<T> {
  return new Promise((resolve, reject) => {
    const proc = spawn("npm", args.concat("--json"), { cwd });

    let result = "";

    proc.stdout.on("data", (chunk) => {
      result += chunk;
    });

    proc.on("close", () => {
      resolve(JSON.parse(result));
    });
  });
}

(async () => {
  const targetPackageJson = process.argv[2];
  const targetDir = path.parse(targetPackageJson).dir;
  const result: Result = {
    dependencies: [],
    devDependencies: [],
  };

  const npmls = await execNpm<{
    name: string;
    dependencies: { [pkgName: string]: { version: string; resolved: string } };
  }>(["list"], targetDir);
  const pkgNames = Object.keys(npmls.dependencies);
  const packageJsonText = await fs.promises.readFile(targetPackageJson, {
    encoding: "utf-8",
  });
  const packageJson = JSON.parse(packageJsonText);
  const dependencies = packageJson.dependencies;
  const devDependencies = packageJson.devDependencies
    ? packageJson.devDependencies
    : [];
  const dependencyNames = Object.keys(dependencies);
  const devDependencyNames = Object.keys(devDependencies);
  const pClient = puppeteerClient();

  for (const pkgName of pkgNames) {
    if (dependencyNames.indexOf(pkgName) >= 0) {
      const pkgViewResult = await execNpm<NpmViewResult>(
        ["view", pkgName],
        targetDir
      );
      const weeklyDownloads = await pClient.getWeeklyDownloads(
        `https://www.npmjs.com/package/${pkgName}`,
        "p._9ba9a726"
      );

      const pkgInfo: PkgInfo = {
        name: pkgName,
        description: pkgViewResult.description,
        latestVersion: pkgViewResult.version,
        homepage: pkgViewResult.homepage,
        usingVersion: npmls.dependencies[pkgName].version,
        weeklyDownloads,
        packageJsonValue: dependencies[pkgName],
        latestRequiredNodeVersion: pkgViewResult.engines
          ? pkgViewResult.engines.node
            ? pkgViewResult.engines.node
            : ""
          : "",
      };
      result.dependencies.push(pkgInfo);
    } else if (devDependencyNames.indexOf(pkgName) >= 0) {
      const pkgViewResult = await execNpm<NpmViewResult>(
        ["view", pkgName],
        targetDir
      );
      const weeklyDownloads = await pClient.getWeeklyDownloads(
        `https://www.npmjs.com/package/${pkgName}`,
        "p._9ba9a726"
      );
      const pkgInfo: PkgInfo = {
        name: pkgName,
        description: pkgViewResult.description,
        latestVersion: pkgViewResult.version,
        homepage: pkgViewResult.homepage,
        usingVersion: npmls.dependencies[pkgName].version,
        weeklyDownloads,
        packageJsonValue: devDependencies[pkgName],
        latestRequiredNodeVersion: pkgViewResult.engines
          ? pkgViewResult.engines.node
            ? pkgViewResult.engines.node
            : ""
          : "",
      };
      result.devDependencies.push(pkgInfo);
    } else {
    }
  }

  const markdownText: string[] = [
    "# Package Using Pakages",
    "",
    `${new Date()}`,
    "",
    "## Devendencies",
    "",
    "| Name | Using Version | Latest Version | Pakage.json Value | Description | Weekly Downloads | Latest Required Node Version | HomePage | ",
    "|:-----|:--------------|:---------------|:------------------|:------------|:-----------------|:-----------------------------|:---------|",
  ];
  const csv: string[][] = [
    [
      "Name",
      "Using Version",
      "Latest Version",
      "Pakage.json Value",
      "Description",
      "Weekly Downloads",
      "Latest Required Node Version",
      "HomePage",
    ],
  ];

  for (const pkgInfo of result.dependencies) {
    markdownText.push(
      `|${pkgInfo.name}|${pkgInfo.usingVersion}|${pkgInfo.latestVersion}|${pkgInfo.packageJsonValue}|${pkgInfo.description}|${pkgInfo.weeklyDownloads}|${pkgInfo.latestRequiredNodeVersion}|${pkgInfo.homepage}|`
    );
    csv.push([
      pkgInfo.name,
      pkgInfo.usingVersion,
      pkgInfo.latestVersion,
      pkgInfo.packageJsonValue,
      pkgInfo.description,
      pkgInfo.weeklyDownloads,
      pkgInfo.latestRequiredNodeVersion,
      pkgInfo.homepage,
    ]);
  }
  markdownText.push("");
  markdownText.push("## DevDependencies");
  markdownText.push(
    "| Name | Using Version | Latest Version | Pakage.json Value | Description | Weekly Downloads | Latest Required Node Version | HomePage | "
  );
  markdownText.push(
    "|:-----|:--------------|:---------------|:------------------|:------------|:-----------------|:-----------------------------|:---------|"
  );

  for (const pkgInfo of result.devDependencies) {
    markdownText.push(
      `|${pkgInfo.name}|${pkgInfo.usingVersion}|${pkgInfo.latestVersion}|${pkgInfo.packageJsonValue}|${pkgInfo.description}|${pkgInfo.weeklyDownloads}|${pkgInfo.latestRequiredNodeVersion}|${pkgInfo.homepage}|`
    );
    csv.push([
      pkgInfo.name,
      pkgInfo.usingVersion,
      pkgInfo.latestVersion,
      pkgInfo.packageJsonValue,
      pkgInfo.description,
      pkgInfo.weeklyDownloads,
      pkgInfo.latestRequiredNodeVersion,
      pkgInfo.homepage,
    ]);
  }

  markdownText.push("");
  const markdown = markdownText.join("\n");

  await fs.promises.writeFile("./result.md", markdown);

  const html = marked(markdown);

  const frame = await fs.promises.readFile("./result-frame.html", {
    encoding: "utf-8",
  });
  const outHtml = frame.replace("$$", html);
  await fs.promises.writeFile("./result.html", outHtml);

  await fs.promises.writeFile('result.csv', csv.map(line => line.map(v => `"${v}"`).join(',')).join('\n'));

  await pClient.close();
})();
